# Make Ends
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5a7f9ac-0526-4c17-8dfe-3abddad23dc7
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/MakeEnds
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create an integer array method called `MakeEnds` that takes in an integer array `nums`. Return a new array length 2 containing the first and last elements from `nums`. You can assume `nums` is length 1 or more.

For example:
```
MakeEnds({1, 2, 3}) → {1, 3}
MakeEnds({1, 2, 3, 4}) → {1, 4}
MakeEnds({7, 4, 6, 2}) → {7, 2}
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public int[] MakeEnds(int[] nums)
{
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
