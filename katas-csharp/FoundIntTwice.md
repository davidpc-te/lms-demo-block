# Found Int Twice
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5801c3a-2e15-4157-b706-7c3673893fd8
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/FoundIntTwice
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a boolean method called `FoundIntTwice` that takes in a List of integers called `nums` and an integer `value`. Return true if `value` appears two or more times in `nums`. Otherwise return false.

For example:
```
FoundIntTwice( [5, 7, 9, 5, 11], 5 ) → true
FoundIntTwice( [6, 8, 10, 11, 13], 8 ) → false
FoundIntTwice( [9, 23, 44, 2, 88, 44], 44 ) → true
```
##### !end-question

##### !placeholder


```
using System;
using System.Collections.Generic;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public bool FoundIntTwice(List<int> nums)
{
    return false;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
