# Bigger Two
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c55b0a63-c040-49cf-b2e6-fef5b9a1c9e6
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/BiggerTwo
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create an integer array method called `BiggerTwo` that takes in two integer arrays `a` and `b`, each of length 2. Return the array which has the largest sum. In the event of a tie, return `a`.

For example:
```
BiggerTwo({1, 2}, {3, 4}) → {3, 4}
BiggerTwo({3, 4}, {1, 2}) → {3, 4}
BiggerTwo({3, 1}, {2, 2}) → {3, 1}
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public int[] BiggerTwo(int[] a, int[] b)
{
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
