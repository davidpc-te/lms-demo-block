# Fizz Buzz
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5443e20-534e-41bf-9ac1-1e521c1e01e3
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/FizzBuzz
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a string array method called `FizzBuzz` with no parameters. Return an array of 100 strings representing the values 1-100. If the value is a multiple of both 3 and 5, put “FizzBuzz” in the array. If the value is a multiple of 3 (but not 5), put “Fizz” in the array. If the value is a multiple of 5 (but not 3), put “Buzz” in the array. For all other values, put a string containing the value in the array.

For example:
```
FizzBuzz() → {"1", "2", "Fizz", "4", "Buzz", "Fizz", "7", ...}
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public string[] FizzBuzz()
{
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
