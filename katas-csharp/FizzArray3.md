# Fizz Array 3
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5f99eed-6839-449b-831f-c4f7f0da2014
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/FizzArray3
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create an integer array method called `FizzArray3` that takes in two integers `start` and `end`. Return a new array containing the sequence of integers from `start` up to but not including `end`. You can assume `end` is greater than or equal to `start`. 

For example:
```
FizzArray3(5, 10) → {5, 6, 7, 8, 9}
FizzArray3(11, 12) → {11}
FizzArray3(3, 3) → {}
```

##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public int[] FizzArray3(int start, int end)
{
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
