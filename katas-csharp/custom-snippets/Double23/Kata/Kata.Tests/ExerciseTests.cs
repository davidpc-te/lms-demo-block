using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.Double23(new int[] { 2, 2 }), "Testing with {2, 2}");
            Assert.AreEqual(true, exercise.Double23(new int[] { 3, 3 }), "Testing with {3, 3}");
            Assert.AreEqual(false, exercise.Double23(new int[] { 2, 3 }), "Testing with {2, 3}");
        }
    }
}
