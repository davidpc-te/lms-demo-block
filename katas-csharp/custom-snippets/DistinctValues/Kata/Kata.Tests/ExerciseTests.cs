using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new List<string> { "red", "yellow", "green", "blue", "purple" }, exercise.DistinctValues(new List<string> { "red", "yellow", "green", "yellow", "blue", "green", "purple" }), "Testing with {'red', 'yellow', 'green', 'yellow', 'blue', 'green', 'purple'}");
            CollectionAssert.AreEqual(new List<string> { "jingle", "bells", "all", "the", "way" }, exercise.DistinctValues(new List<string> { "jingle", "bells", "jingle", "bells", "jingle", "all", "the", "way" }), "Testing with {'jingle', 'bells', 'jingle', 'bells', 'jingle', 'all', 'the', 'way'}");
        }
    }
}
