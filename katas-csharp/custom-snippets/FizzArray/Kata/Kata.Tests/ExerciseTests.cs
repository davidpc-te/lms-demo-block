using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new int[] { 0, 1, 2, 3 }, exercise.FizzArray(4), "Testing with 4");
            CollectionAssert.AreEqual(new int[] { 0 }, exercise.FizzArray(1), "Testing with 1");
            CollectionAssert.AreEqual(new int[] { }, exercise.FizzArray(0), "Testing with 0");
        }
    }
}
