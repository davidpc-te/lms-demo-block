using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new List<int> { 201, 9, 83 }, exercise.OddOnly(new int[] { 112, 201, 774, 92, 9, 83, 41872 }), "Testing with {112, 201, 774, 92, 9, 83, 41872}");
            CollectionAssert.AreEqual(new List<int> { 1143, 555, 7, 9953, 643 }, exercise.OddOnly(new int[] { 1143, 555, 7, 1772, 9953, 643 }), "Testing with {1143, 555, 7, 1772, 9953, 643}");
            CollectionAssert.AreEqual(new List<int> { 233, 811, 3, 9999 }, exercise.OddOnly(new int[] { 734, 233, 782, 811, 3, 9999 }), "Testing with {734, 233, 782, 811, 3, 9999}");
        }
    }
}
