using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(1, exercise.ArrayCount9(new int[] { 1, 2, 9 }), "Testing with {1, 2, 9}");
            Assert.AreEqual(2, exercise.ArrayCount9(new int[] { 1, 9, 9 }), "Testing with {1, 9, 9}");
            Assert.AreEqual(3, exercise.ArrayCount9(new int[] { 1, 9, 9, 3, 9 }), "Testing with {1, 9, 9, 3, 9}");
        }
    }
}
