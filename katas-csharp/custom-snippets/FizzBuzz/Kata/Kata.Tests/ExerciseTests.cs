using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            string[] result = exercise.FizzBuzz();
            CollectionAssert.Contains(result, "1", "Didn't find 1");
            CollectionAssert.Contains(result, "2", "Didn't find 2");
            CollectionAssert.Contains(result, "Fizz", "Didn't find Fizz");
            CollectionAssert.Contains(result, "4", "Didn't find 4");
            CollectionAssert.Contains(result, "Buzz", "Didn't find Buzz");
            CollectionAssert.Contains(result, "7", "Didn't find 7");
            CollectionAssert.Contains(result, "8", "Didn't find 8");
            CollectionAssert.Contains(result, "FizzBuzz", "Didn't find FizzBuzz");
            Assert.AreEqual("Fizz", result[2], "Didn't find Fizz at index 2");
            Assert.AreEqual("Buzz", result[4], "Didn't find Buzz at index 4");
            Assert.AreEqual("FizzBuzz", result[14], "Didn't find FizzBuzz at index 14");
        }
    }
}
