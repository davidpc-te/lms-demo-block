using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            CollectionAssert.AreEqual(new int[] {0, 0, 0, 0, 0, 6 }, exercise.MakeLast(new int[] { 4, 5, 6 }), "Testing with {4, 5, 6}");
            CollectionAssert.AreEqual(new int[] { 0, 0, 0, 2 }, exercise.MakeLast(new int[] { 1, 2 }), "Testing with {1, 2}");
            CollectionAssert.AreEqual(new int[] { 0, 3}, exercise.MakeLast(new int[] { 3 }), "Testing with {3}");
        }
    }
}
