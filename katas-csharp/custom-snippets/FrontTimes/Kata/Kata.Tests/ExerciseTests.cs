using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual("ChoCho", exercise.FrontTimes("Chocolate", 2), "Testing with 'Chocolate', 2");
            Assert.AreEqual("ChoChoCho", exercise.FrontTimes("Chocolate", 3), "Testing with 'Chocolate', 3");
            Assert.AreEqual("AbAbAb", exercise.FrontTimes("Ab", 3), "Testing with 'Ab', 3");
        }
    }
}
