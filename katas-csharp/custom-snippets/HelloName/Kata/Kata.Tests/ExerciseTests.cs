using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual("Hello Bob!", exercise.HelloName("Bob"), "Testing with 'Bob'");
            Assert.AreEqual("Hello Alice!", exercise.HelloName("Alice"), "Testing with 'Alice'");
            Assert.AreEqual("Hello X!", exercise.HelloName("X"), "Testing with 'V'");
        }
    }
}
