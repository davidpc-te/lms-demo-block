using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual("!olleH", exercise.ReverseString("Hello!"), "Testing with Hello");
            Assert.AreEqual("ataK", exercise.ReverseString("Kata"), "Testing with Kata");
            Assert.AreEqual("", exercise.ReverseString(""), "Testing with empty string");
        }
    }
}
