using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(33333, exercise.FindLargest(new List<int> { 11, 200, 43, 84, 9917, 4321, 1, 33333, 8997 }), "Testing with {11, 200, 43, 84, 9917, 4321, 1, 33333, 8997}");
            Assert.AreEqual(43718, exercise.FindLargest(new List<int> { 987, 1234, 9381, 731, 43718, 8932 }), "Testing with {987, 1234, 9381, 731, 43718, 8932}");
            Assert.AreEqual(81238, exercise.FindLargest(new List<int> { 34070, 1380, 81238, 7782, 234, 64362, 627 }), "Testing with {34070, 1380, 81238, 7782, 234, 64362, 627}");
        }
    }
}
