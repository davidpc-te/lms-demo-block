using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(6, exercise.Factorial(3), "Testing with 3");
            Assert.AreEqual(24, exercise.Factorial(4), "Testing with 4");
            Assert.AreEqual(3628800, exercise.Factorial(10), "Testing with 10");
       }
    }
}
