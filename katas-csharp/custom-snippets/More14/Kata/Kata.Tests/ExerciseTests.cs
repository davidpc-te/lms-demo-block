using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.More14(new int[] { 1, 4, 1 }), "Testing with {1, 4, 1}");
            Assert.AreEqual(false, exercise.More14(new int[] { 1, 2, 3, 4 }), "Testing with {1, 2, 3, 4}");
            Assert.AreEqual(false, exercise.More14(new int[] {2, 3, 4}), "Testing with {2, 3, 4}");
        }
    }
}
