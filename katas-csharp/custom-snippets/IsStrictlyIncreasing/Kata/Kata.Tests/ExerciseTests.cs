using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kata.Tests
{
    [TestClass]
    public class ExerciseTests
    {
        Exercise exercise = new Exercise();

        [TestMethod]
        public void VerifyOutput()
        {
            Assert.AreEqual(true, exercise.IsStrictlyIncreasing(new int[] { 5, 7, 8, 10 }), "Testing with {5,7,8,10}");
            Assert.AreEqual(false, exercise.IsStrictlyIncreasing(new int[] { 5, 7, 7, 10 }), "Testing with {5,7,7,10}");
            Assert.AreEqual(true, exercise.IsStrictlyIncreasing(new int[] { -5, -3, 0, 17 }), "Testing with {-5,-3,0,17}");
        }
    }
}
