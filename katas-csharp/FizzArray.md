# Fizz Array
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5413a89-b166-4b4b-bb4e-3bbfaf706aa3
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/FizzArray
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create an integer method called `FizzArray` that takes in an integer `n`. Return an array of length `n`, containing the integers 0, 1, 2, ... n-1. `n` may be 0, in which case return a length 0 array.

For example:
```
FizzArray(4) → {0, 1, 2, 3}
FizzArray(1) → {0}
FizzArray(0) → {}
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public int[] FizzArray(int n)
{
    return null;
}
```
##### !end-hint

##### !hint
You don't need a separate if-statement for the length-0 case--the for loop executes 0 times in that case, so it just works.
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
