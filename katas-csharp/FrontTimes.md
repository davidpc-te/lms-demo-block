# Front Times
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c5411efc-fa74-4735-81d7-a7098449f0fe
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/FrontTimes
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a string method called `FrontTimes` that takes in a string `str` and an integer `n`. The front of `str` is the first 3 characters, or whatever is there if `str` is less than length 3. Return a string made up of `n` copies of the front.

For example:
```
FrontTimes("Chocolate", 2) → "ChoCho"
FrontTimes("Chocolate", 3) → "ChoChoCho"
FrontTimes("Ab", 3) → "AbAbAb"
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public string FrontTimes(string str, int n)
{
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
