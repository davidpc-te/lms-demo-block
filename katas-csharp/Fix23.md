# Fix 2 3
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: csharp
* id: c57c2ae8-1ca5-44fc-a3c7-759545899ecb
* title: C# Kata
* docker_directory_path: /katas-csharp/custom-snippets/Fix23
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create an integer array method called `Fix23` that takes in an array called `nums` that contains 3 integers. If there is a 2 in the array immediately followed by a 3, change the 3 to 0. Return the changed array.

For example:
```
Fix23({1, 2, 3}) → {1, 2, 0}
Fix23({2, 3, 5}) → {2, 0, 5}
Fix23({1, 2, 1}) → {1, 2, 1}
```
##### !end-question

##### !placeholder


```
using System;

namespace Kata
{
    public class Exercise
    {
      /*
      public <type> <name>(<parameters>)
      {
        <code>
      }
      */
    }
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```csharp
public int[] Fix23(int[] nums)
{
    return nums;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
