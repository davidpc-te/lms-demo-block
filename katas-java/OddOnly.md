# Odd Only
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 786b7a3e-5820-4fa1-a8dc-0ceb28ae315e
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/OddOnly
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `oddOnly` that takes in an integer array called `nums`. Return a List of integers containing just the odd values in `nums`.

For example:
```
oddOnly( {112, 201, 774, 92, 9, 83, 41872} ) →  [201, 9, 83]
oddOnly( {1143, 555, 7, 1772, 9953, 643} ) →  [1143, 555, 7, 9953, 643]
oddOnly( {734, 233, 782, 811, 3, 9999} ) →  [233, 811, 3, 9999] 
```
##### !end-question

##### !placeholder


```
import java.util.List;
import java.util.ArrayList;

public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public List<Integer> oddOnly(int[] nums) {
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
