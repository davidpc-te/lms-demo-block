# Swap Ends
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: d5a017c2-fda1-4c37-b6c5-692e6a5170ac
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/SwapEnds
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `swapEnds` that takes in an integer array `nums`. Swap the first and last elements in `nums`. Return the modified array. You can assume the length of `nums` is at least 1.

For example:
```
swapEnds({1, 2, 3, 4}) → {4, 2, 3, 1}
swapEnds({1, 2}) → {2, 1}
swapEnds({8}) → {8}
```
##### !end-question

##### !placeholder


```
public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public int[] swapEnds(int[] nums) {
  return nums;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
