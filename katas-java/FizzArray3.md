# Fizz Array 3
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 59f99eed-6839-449b-831f-c4f7f0da2014
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/FizzArray3
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `fizzArray3` that takes in two integers `start` and `end`. Return a new array containing the sequence of integers from `start` up to but not including `end`. You can assume `end` is greater than or equal to `start`. 

For example:
```
fizzArray3(5, 10) → {5, 6, 7, 8, 9}
fizzArray3(11, 12) → {11}
fizzArray3(3, 3) → {}
```

##### !end-question

##### !placeholder


```
public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public int[] fizzArray3(int start, int end) {
    return null;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
