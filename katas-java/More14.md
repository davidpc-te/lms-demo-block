# More 1 4
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: cecbc8f5-e8f5-49b0-a628-5856ce939f01
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/More14
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `more14` that takes in an array of integers called `nums`. Return true if the number of 1s in `nums` is greater than the number of 4s in `nums`. Otherwise return false.


For example:
```
more14({1, 4, 1}) → true
more14({1, 4, 1, 4}) → false
more14({1, 1}) → true
```
##### !end-question

##### !placeholder


```
public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public boolean more14(int[] nums) {
    return false;
}
```
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
