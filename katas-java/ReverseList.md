# Reverse List
<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->
<!-- Replace everything in square brackets [] and remove brackets  -->

### !challenge

* type: custom-snippet
* language: java
* id: 11c55d5d-9991-4c04-ac02-be0f6b448f3e
* title: Java Kata
* docker_directory_path: /katas-java/custom-snippets/ReverseList
<!-- * points: [1] (optional, the number of points for scoring as a checkpoint) -->
<!-- * topics: [python, pandas] (Checkpoints only, optional the topics for analyzing points) -->

##### !question

Create a method called `reverseList` that takes in a List of strings called `strings`. Return a new List in reverse order of the original.

For example:
```
reverseList( ["purple", "green", "blue", "yellow", "green" ])  →  ["green", "yellow", "blue", green", "purple" ]
reverseList( ["jingle", "bells", "jingle", "bells", "jingle", "all", "the", "way"} ) →  ["way", "the", "all", "jingle", "bells", "jingle", "bells", "jingle"]
```
##### !end-question

##### !placeholder


```
import java.util.List;
import java.util.ArrayList;

public class Kata {

    /*
    public <type> <name>(<parameters>) {
      <code>
    }
    */
}
```

##### !end-placeholder

##### !hint
Here is a method to use as a starting point:
```java
public List<String> reverseList(List<String> strings) {
    return null;
}
```
##### !end-hint
##### !hint
Think `Stack`.
##### !end-hint

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->
