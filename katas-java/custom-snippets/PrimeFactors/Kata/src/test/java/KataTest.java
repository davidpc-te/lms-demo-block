import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertArrayEquals("Testing with 6", new int[] {2, 3} , kata.primeFactors(6));
        assertArrayEquals("Testing with 28", new int[] {2, 2, 7}, kata.primeFactors(28));
        assertArrayEquals("Testing with 667", new int[] {23, 29}, kata.primeFactors(667));
    }


}
