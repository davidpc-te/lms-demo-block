import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with \"Hello\", \"hi\"", "hiHellohi", kata.comboString("Hello", "hi"));
        assertEquals("Testing with \"hi\", \"Hello\"", "hiHellohi", kata.comboString("hi", "Hello"));
        assertEquals("Testing with \"aaa\", \"b\"", "baaab", kata.comboString("aaa", "b"));
    }


}
