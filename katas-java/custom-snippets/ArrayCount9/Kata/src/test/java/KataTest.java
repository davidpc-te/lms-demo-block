import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {1, 2, 9}", 1, kata.arrayCount9(new int[] {1, 2, 9}));
        assertEquals("Testing with {1, 9, 9}", 2, kata.arrayCount9(new int[] {1, 9, 9}));
        assertEquals("Testing with {1, 9, 9, 3, 9}", 3, kata.arrayCount9(new int[] {1, 9, 9, 3, 9}));

    }


}
