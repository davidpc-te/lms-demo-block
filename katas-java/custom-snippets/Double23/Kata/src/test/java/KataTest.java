import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {2, 2}", true, kata.double23(new int[] {2, 2}));
        assertEquals("Testing with {3, 3}", true, kata.double23(new int[] {3, 3}));
        assertEquals("Testing with {2, 3}", false, kata.double23(new int[] {2, 3}));
    }


}
