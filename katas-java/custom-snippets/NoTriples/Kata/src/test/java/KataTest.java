import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {1, 1, 2, 2, 1}", true, kata.noTriples(new int[] {1, 1, 2, 2, 1}));
        assertEquals("Testing with {1, 1, 2, 2, 2, 1}", false, kata.noTriples(new int[] {1, 1, 2, 2, 2, 1}));
        assertEquals("Testing with {1, 1, 1, 2, 2, 2, 1}", false, kata.noTriples(new int[] {1, 1, 1, 2, 2, 2, 1}));
    }


}
