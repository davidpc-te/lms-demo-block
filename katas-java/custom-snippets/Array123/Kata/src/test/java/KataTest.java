import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {1, 1, 2, 3, 1}", true, kata.array123(new int[] {1, 1, 2, 3, 1}));
        assertEquals("Testing with {1, 1, 2, 4, 3}", false, kata.array123(new int[] {1, 1, 2, 4, 3}));
        assertEquals("Testing with {1, 1, 2, 1, 2, 3}", true, kata.array123(new int[] {1, 1, 2, 1, 2, 3}));

    }


}
