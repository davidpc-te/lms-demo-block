import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with \"Bob\")", "Hello Bob!", kata.helloName("Bob"));
        assertEquals("Testing with \"Alice\"", "Hello Alice!", kata.helloName("Alice"));
        assertEquals("Testing with \"X\"", "Hello X!", kata.helloName("X"));
    }


}
