import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with \"axxbb\"", true, kata.doubleX("axxbb"));
        assertEquals("Testing with \"axaxax\"", false, kata.doubleX("axaxax"));
        assertEquals("Testing with \"xxxxx\"", true, kata.doubleX("xxxxx"));
    }


}
