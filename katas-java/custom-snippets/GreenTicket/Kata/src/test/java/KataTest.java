import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with (1, 2, 3)", 0, kata.greenTicket(1, 2, 3));
        assertEquals("Testing with (2, 2, 2)", 20, kata.greenTicket(2, 2, 2));
        assertEquals("Testing with (1, 1, 2)", 10, kata.greenTicket(1, 1, 2));
    }


}
