import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {1, 4, 1, 4}", true, kata.only14(new int[] {1, 4, 1, 4}));
        assertEquals("Testing with {1, 4, 2, 4}", false, kata.only14(new int[] {1, 4, 2, 4}));
        assertEquals("Testing with {1, 1}", true, kata.only14(new int[] {1, 1}));

    }


}
