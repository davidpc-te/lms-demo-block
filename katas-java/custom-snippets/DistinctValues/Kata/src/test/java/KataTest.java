import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        List<String> actual = kata.distinctValues(Arrays.asList("red", "yellow", "green", "yellow", "blue", "green", "purple"));
        List<String> expected = Arrays.asList("red", "yellow", "green",  "blue", "purple");
        // Implementation may have removed duplicates using a HashSet which is unordered.
        // Sort the Lists alphabetically prior to the assertEquals
        Collections.sort(actual);
        Collections.sort(expected);
        assertEquals("Testing with {\"red\", \"yellow\", \"green\", \"yellow\", \"blue\", \"green\", \"purple\"}",
                expected, actual);

        actual = kata.distinctValues(Arrays.asList("jingle", "bells", "jingle", "bells", "jingle", "all", "the", "way"));
        expected = Arrays.asList("jingle", "bells", "all", "the", "way");
        Collections.sort(actual);
        Collections.sort(expected);
        assertEquals("Testing with {\"jingle\", \"bells\", \"jingle\", \"bells\", \"jingle\", \"all\", \"the\", \"way\"}",
                expected, actual);
    }


}
