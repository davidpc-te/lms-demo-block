import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {1, 4, 1}", true, kata.more14(new int[] {1, 4, 1}));
        assertEquals("Testing with {1, 4, 1, 4}", false, kata.more14(new int[] {1, 4, 1, 4}));
        assertEquals("Testing with {1, 1}", true, kata.more14(new int[] {1, 1}));

    }


}
