import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with 3", 6, kata.factorial(3));
        assertEquals("Testing with 4", 24, kata.factorial(4));
        assertEquals("Testing with 10", 3628800, kata.factorial(10));
    }


}
