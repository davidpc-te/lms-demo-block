import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with {\"aa\", \"ab\", \"ac\"}",
                new String[] {"aa", "ab", "ac"},
                kata.list2Array(Arrays.asList("aa", "ab", "ac")));

        assertEquals("Testing with {\"as\", \"df\", \"jk\"}",
                new String[] {"as", "df", "jk"},
                kata.list2Array(Arrays.asList("as", "df", "jk")));

        assertEquals("Testing with {\"aaa\", \"bbb\", \"ccc\", \"ddd\"]",
                new String[] {"aaa", "bbb", "ccc", "ddd"},
                kata.list2Array(Arrays.asList("aaa", "bbb", "ccc", "ddd")));
    }


}
