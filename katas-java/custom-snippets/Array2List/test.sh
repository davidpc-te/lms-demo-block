#!/bin/sh
submission_file=${1:-submission.txt}

cat prefix "$submission_file" > src/main/java/Kata.java

mvn test -e -B | tr -d '\000' > output.txt

# Get the total number of test methods in the source files
shopt -s globstar # causes ** in the next line to dive into subfolders
# Look for the @Test annotation indicating we found a test method
number_of_tests=$(grep -o -i -w -a @Test src/test/**/*.java | wc -l)


# Get the results from the output file. 'tac' reverses the file, so we find the last line that contains the text.
# The last line is a summary line, in case there are many test classes
number_of_run_tests=$(tac output.txt | grep -P -o -a "(?<=Tests run: )(\d+)" | head -1)

# The tests didn't run and something else is wrong
if [ -z "$number_of_run_tests" ]
then
    echo "An error prevented your code from being tested."
    echo "Make sure: "
    echo "* your method has the correct method signature and return type"
    echo "* your method is contained in a class named Kata"
    echo "* all parentheses and brackets are in matching pairs"
    echo "* your method returns a value of the declared type"

    exit 1
fi

# Continue finding results
number_failed=$(tac output.txt | grep -P -o -a "(?<=Failures: )(\d+)" | head -1)
number_errors=$(tac output.txt | grep -P -o -a "(?<=Errors: )(\d+)" | head -1)
number_skipped=$(tac output.txt | grep -P -o -a "(?<=Skipped: )(\d+)" | head -1)
number_passed=$((number_of_run_tests-(number_failed+number_errors+number_skipped)))


if [ $number_passed = $number_of_tests ]
then
    echo "All tests passed."
    exit 0
else
    grep 'Failed tests:' output.txt
    exit 1
fi
