import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with [112, 201, 774, 92, 9, 83, 41872]",
                Arrays.asList(201, 9, 83),
                kata.oddOnly(new int [] {112, 201, 774, 92, 9, 83, 41872}));

        assertEquals("Testing with [1143, 555, 7, 1772, 9953, 643]",
                Arrays.asList(1143, 555, 7, 9953, 643),
                kata.oddOnly(new int [] {1143, 555, 7, 1772, 9953, 643}));

        assertEquals("Testing with [734, 233, 782, 811, 3, 9999]",
                Arrays.asList(233, 811, 3, 9999),
                kata.oddOnly(new int [] {734, 233, 782, 811, 3, 9999}));
    }


}
