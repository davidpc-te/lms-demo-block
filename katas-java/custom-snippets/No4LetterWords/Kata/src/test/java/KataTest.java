import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with [\"Train\", \"Boat\", \"Car\"]",
                Arrays.asList("Train", "Car"),
                kata.no4LetterWords(new String[] {"Train", "Boat", "Car"}));

        assertEquals("Testing with [\"Red\", \"White\", \"Blue\"]",
                Arrays.asList("Red", "White"),
                kata.no4LetterWords(new String[] {"Red", "White", "Blue"}));
    }


}
