import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with \"<<>>\", \"Yay\"",
                "<<Yay>>", kata.makeOutWord("<<>>", "Yay"));

        assertEquals("Testing with \"<<>>\", \"WooHoo\"",
                "<<WooHoo>>", kata.makeOutWord("<<>>", "WooHoo"));

        assertEquals("Testing with \"[[]]\", \"word\"",
                "[[word]]", kata.makeOutWord("[[]]", "word"));
    }


}
