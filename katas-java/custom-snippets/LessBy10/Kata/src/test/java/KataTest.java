import org.junit.Test;

import static org.junit.Assert.*;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertEquals("Testing with (1, 7, 11)", true, kata.lessBy10(1, 7, 11));
        assertEquals("Testing with (1, 7, 10)", false, kata.lessBy10(1, 7, 10));
        assertEquals("Testing with (11, 1, 7)", true, kata.lessBy10(11, 1, 7));
    }


}
