import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class KataTest {

    Kata kata = new Kata();

    @Test
    public void verifyOutput() {
        assertArrayEquals("Testing with {1, 2, 3, 4}", new int[] {4, 2, 3, 1} , kata.swapEnds(new int[] {1, 2, 3, 4}));
        assertArrayEquals("Testing with {1, 2, 3}", new int[] {3, 2, 1} , kata.swapEnds(new int[] {1, 2, 3}));
        assertArrayEquals("Testing with {8, 6, 7, 9, 5}", new int[] {5, 6, 7, 9, 8} , kata.swapEnds(new int[] {8, 6, 7, 9, 5}));
    }


}
