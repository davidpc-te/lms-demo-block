<!-- >>>>>>>>>>>>>>>>>>>>>> BEGIN CHALLENGE >>>>>>>>>>>>>>>>>>>>>> -->

### !challenge

* type: testable-project
* id: 3513a582-cbe1-4428-bccd-28430acbbbf1
* title: Exercise for File I/O - Reading
* upstream: https://gitlab.com/te-curriculum/file-io-java-lms/-/tree/release_v1_0/__student/02_File_IO_Reading/exercise
* validate_fork: false
* points: 3
<!-- * topics: [python, pandas] (optional the topics for analyzing points) -->

##### !question

1. Find the `README.md` file in the `exercise` folder for this unit.
2. Open the `README.md` file in your preferred Markdown viewer.
3. Follow the instructions in the `README.md` file to complete the exercise.
4. When all tests pass, commit and push your changes.
5. In your browser, navigate to the current exercise in your repository.
6. Copy and paste the URL from the browser into the textbox below.
7. Click **Submit** to submit your exercise.

##### !end-question

##### !placeholder

Enter the URL from step six here.

##### !end-placeholder

<!-- other optional sections -->
<!-- !hint - !end-hint (markdown, hidden, students click to view) -->
<!-- !rubric - !end-rubric (markdown, instructors can see while scoring a checkpoint) -->
<!-- !explanation - !end-explanation (markdown, students can see after answering correctly) -->

### !end-challenge

<!-- ======================= END CHALLENGE ======================= -->